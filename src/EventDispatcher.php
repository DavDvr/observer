<?php

namespace DDevaure\Observer;

use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\EventDispatcher\ListenerProviderInterface;
use Psr\EventDispatcher\StoppableEventInterface;

class EventDispatcher implements EventDispatcherInterface
{

    private  ListenerProviderInterface $listenerProvider;

    /**
     * EventDispatcher constructor.
     * @param ListenerProviderInterface $listenerProvider
     */
    public function __construct(ListenerProviderInterface $listenerProvider)
    {
        $this->listenerProvider = $listenerProvider;
    }


    public function dispatch(object $event): void
    {
        foreach ($this->listenerProvider->getListenersForEvent($event) as $listener){
            $listener($event);

            /**
             * si mon évènement implémente la StoppableEventInterface::class
             * et que j'appelle la methode isPropagationStopped, je break
             */
            if (in_array(StoppableEventInterface::class,class_implements($event))
                && $event->isPropagationStopped()
            ){
                break;
            }
        }
    }
}
<?php

namespace DDevaure\Observer\Tests;

use DDevaure\Observer\EventDispatcher;
use DDevaure\Observer\Tests\Fixtures\BarEvent;
use DDevaure\Observer\Tests\Fixtures\FooEvent;
use PHPUnit\Framework\TestCase;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\EventDispatcher\ListenerProviderInterface;

class EventDispatcherTest extends TestCase
{
    private EventDispatcherInterface $eventDispatcher;

    /**
     * methode appelée avant tout test
     */
    public function setUp(): void
    {
        $this->eventDispatcher = new EventDispatcher(new class() implements ListenerProviderInterface{

            public function getListenersForEvent(object $event): iterable
            {
                $listeners = [
                    FooEvent::class => [
                        function (FooEvent $fooEvent){
                            $fooEvent->setTest("bar");
                        },
                        function (FooEvent $fooEvent){
                            $fooEvent->setTest("room");
                        }
                    ],
                    BarEvent::class => [

                        function (BarEvent $barEvent){
                            $barEvent->setTest("foo");
                        },
                        function (BarEvent $barEvent){
                            $barEvent->setTest("great");
                        }
                    ]
                ];

                return $listeners[$event::class];
            }
        });
    }

    /**
     * test du dispatch
     */
    public function testIfDispatchIsSuccessfully(): void
    {

        //Event FooEvent
        $event = new FooEvent("foo");

        //je m'attends a voir foo
        $this->assertEquals("foo", $event->getTest());

        $this->eventDispatcher->dispatch($event);
        //je mattends a voir room
        $this->assertEquals("room", $event->getTest());

    }

    /**
     * test avec le StoppableEvent
     * great ne sera jamais appelé car isPropagationStopped est a true
     *
     */
    public function testIsStoppableEventIsWorking(): void
    {
        //Event BarEvent
        $eventbar = new BarEvent("bar");

        //je m'attends à avoir bar
        $this->assertEquals("bar", $eventbar->getTest());

        $this->eventDispatcher->dispatch($eventbar);
        //je m'attends à avoir foo et great ne sera jamais appelé
        $this->assertEquals("foo", $eventbar->getTest());
    }
}